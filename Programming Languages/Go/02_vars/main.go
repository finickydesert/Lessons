package main

import "fmt"

func main() {
	//var name = "richard"
	var age int32 = 37
	const isCool = true
	var size float32 = 2.3
//shorthand
name:= "richard"
email := "rherist@gmail.com"
	fmt.Println(name, age, isCool, size, email)
	fmt.Printf("%T\n", size)


}
